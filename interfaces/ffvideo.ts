export interface Iffvideo {
    bin: string

}
export interface IffvideoConf {
    bin?: string
    destinationPath?: string
}

export interface IVideoParams {
    bin?: string
    audioCodec?: string
    videoCodec?: string
    resolution?: string
    startTime?: number
    stopTime?: number
    duration?: number
    name?:string
    destinationPath?:string
}



export interface Ivideoinfo {
    duration: number
    extension: string
    path: string
    name: string
    fullname: string
}