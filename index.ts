import { Iffvideo, IffvideoConf, IVideoParams, Ivideoinfo } from './interfaces/ffvideo'
import * as child_process from 'child_process'
import * as fs from 'fs'
import * as nodeFileGenSeries from 'node-file-gen-series'

import * as Promise from 'bluebird'

const spawn = child_process.spawn

const getDuration = require('get-video-duration');


export class FFVideoConvert implements Iffvideo {
    bin: string = 'ffmpeg'
    destinationPath: string = '.'

    constructor(conf?: IffvideoConf) {
        const that = this
        if (conf) {
            if (conf.bin) that.bin = conf.bin
            if (conf.destinationPath) that.destinationPath = conf.destinationPath

        }
    }
    getDuration(video: string) {
        return new Promise<number>((resolve, reject) => {
            getDuration(video).then((a) => {
                resolve(a)
            }).catch((err) => {
                reject(err)
            })
        })
    }
    info(video: string) {
        const that = this
        return new Promise<Ivideoinfo>((resolve, reject) => {
            if (fs.existsSync(video)) {
                const info = <Ivideoinfo>{}
                that.getDuration(video).then((a) => {
                    info.duration = a
                    info.fullname = video.split('/')[video.split('/').length - 1]
                    info.path = video.split(info.fullname)[0]
                    info.extension = info.fullname.split('.')[info.fullname.split('.').length - 1]
                    info.name = info.fullname.split('.' + info.extension)[info.fullname.split('.' + info.extension).length - 2]
                    resolve(info)
                }).catch((err) => {
                    reject(err)
                })
            } else {
                reject('file not exists')
            }
        })
    }
    loadcmd(video: string, params: IVideoParams) {
        const that = this
        return new Promise<{ cmd: string, options: string[], destfile: string }>((resolve, reject) => {
            if (!params || !video) {
                reject('need more params')
            } else {

                that.info(video).then((info) => {

                    if (!params.bin) params.bin = that.bin
                    if (!params.destinationPath) params.destinationPath = that.destinationPath
                    const cmd = ['-y', '-i', video,]

                    let name = info.name + '_conv'


                    if (params.name) name = params.name

                    const destfile = params.destinationPath + '/' + name

                    let destfileconverted


                    if (params.startTime) {
                        cmd.push('-ss')
                        cmd.push(params.startTime + '')
                        if (params.stopTime) {
                            cmd.push('-t')
                            cmd.push((params.stopTime - params.startTime) + '')
                        } else if (params.duration) {
                            cmd.push('-t')
                            cmd.push(params.duration + '')
                        }

                    } else if (params.duration) {
                        cmd.push('-t')
                        cmd.push(params.duration + '')
                    } else if (params.stopTime) {
                        cmd.push('-t')
                        cmd.push((params.stopTime - params.startTime) + '')
                    }

                    if (!params.audioCodec && !params.videoCodec) {
                        cmd.push('-c:a')
                        cmd.push('copy')
                        cmd.push('-c:v')
                        cmd.push('copy')

                        if (info.extension && (info.extension === 'mp4' || info.extension === 'flv')) {
                            cmd.push('-f')
                            cmd.push('mp4')
                            destfileconverted = nodeFileGenSeries.genFileSequence(destfile + '.mp4')
                            cmd.push(destfileconverted)
                        }
                    } else if (params.audioCodec === 'none' && !params.videoCodec) {
                        cmd.push('-c:v')
                        cmd.push('copy')
                    } else if (params.videoCodec === 'none' && !params.audioCodec) {
                        cmd.push('-c:a')
                        cmd.push('copy')
                    } else if (params.videoCodec === 'none' && params.audioCodec) {
                        cmd.push('-c:a')
                        cmd.push(params.audioCodec)
                    } else if (params.audioCodec === 'none' && params.videoCodec) {
                        cmd.push('-c:v')
                        cmd.push(params.videoCodec)
                    } else {
                        cmd.push('-c:a')
                        cmd.push(params.audioCodec)
                        cmd.push('-c:v')
                        cmd.push(params.videoCodec)
                    }
                    console.log(cmd)
                    resolve({ cmd: params.bin, options: cmd, destfile: destfileconverted })
                }).catch((err) => {
                    reject(err)
                })



            }


        })

    }

    convert(video: string, params: IVideoParams) {
        return new Promise<string>((resolve, reject) => {
            const that = this
            that.loadcmd(video, params).then((p) => {
                const conversion = spawn(p.cmd, p.options, { detached: true, stdio: 'ignore' })
                conversion.on("close", (code, signal) => {
                    console.log(code, signal)
                    if (code === 0) {
                        resolve(p.destfile)
                    } else {
                        reject({ error: { code: code, signal: signal } })
                    }
                })

            }).catch((err) => {
                reject(err)
            })

        })
    }
    extractAudio(video, destname?: string) {
        const that = this
        return new Promise((resolve, reject) => {
            const newparams: IVideoParams = { audioCodec: 'copy' }
            if (destname) newparams.name = destname
            that.convert(video, newparams).then((a) => {
                resolve(a)
            }).catch((err) => {
                reject(err)
            })
        })
    }
    extractVideo(video, destname?: string) {
        const that = this
        return new Promise((resolve, reject) => {
            const newparams: IVideoParams = { videoCodec: 'copy' }
            if (destname) newparams.name = destname
            that.convert(video, newparams).then((a) => {
                resolve(a)
            }).catch((err) => {
                reject(err)
            })
        })
    }
    cutVideo(video, params: { startTime: number, duration?: number, stopTime?: number, name?: string }) {
        const that = this
        return new Promise((resolve, reject) => {
            const newparams: IVideoParams = params
            if (params.name) newparams.name = params.name
            that.convert(video, newparams).then((a) => {
                resolve(a)
            }).catch((err) => {
                reject(err)
            })
        })
    }
}