import * as chai from 'chai'
const expect = chai.expect

import { FFVideoConvert } from '../index'

let ffmpeg: FFVideoConvert

describe('main test', function () {


    before(function (done) {

        ffmpeg = new FFVideoConvert({ destinationPath: '/tmp/' })

        done()
    })

    describe('config', function () {
        it('it is configured', function (done) {
            expect(ffmpeg).to.be.an('Object')
            expect(ffmpeg.bin).to.be.a('string')
            done()
        });
    });
    describe('get info', function () {
        it('can obtain video info', function (done) {
            ffmpeg.info(__dirname + '/videotest.mp4').then((a) => {
                console.log(a)
                expect(a).to.be.an('Object')
                done()
            }).catch((err) => {
                done(new Error(err))
            })

        });
    });
    describe('cut video', function () {
        this.timeout(20000)
        it('it cut the video', function (done) {
            ffmpeg.cutVideo(__dirname + '/videotest.mp4', { startTime: 100 }).then((a) => {
                console.log(a)
                expect(a).to.be.ok
                done()
            }).catch((err) => {
                done(new Error(err))
            })
        });
    });
    describe('convert with custom params', function () {
        it('it is configured', function (done) {
            expect(ffmpeg).to.be.an('Object')
            expect(ffmpeg.bin).to.be.a('string')
            done()
        });
    });

    describe('convert with custom cmd', function () {
        it('it is configured', function (done) {
            expect(ffmpeg).to.be.an('Object')
            expect(ffmpeg.bin).to.be.a('string')
            done()
        });
    });
})