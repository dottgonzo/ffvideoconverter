"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var child_process = require("child_process");
var fs = require("fs");
var nodeFileGenSeries = require("node-file-gen-series");
var Promise = require("bluebird");
var spawn = child_process.spawn;
var getDuration = require('get-video-duration');
var FFVideoConvert = (function () {
    function FFVideoConvert(conf) {
        this.bin = 'ffmpeg';
        this.destinationPath = '.';
        var that = this;
        if (conf) {
            if (conf.bin)
                that.bin = conf.bin;
            if (conf.destinationPath)
                that.destinationPath = conf.destinationPath;
        }
    }
    FFVideoConvert.prototype.getDuration = function (video) {
        return new Promise(function (resolve, reject) {
            getDuration(video).then(function (a) {
                resolve(a);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    FFVideoConvert.prototype.info = function (video) {
        var that = this;
        return new Promise(function (resolve, reject) {
            if (fs.existsSync(video)) {
                var info_1 = {};
                that.getDuration(video).then(function (a) {
                    info_1.duration = a;
                    info_1.fullname = video.split('/')[video.split('/').length - 1];
                    info_1.path = video.split(info_1.fullname)[0];
                    info_1.extension = info_1.fullname.split('.')[info_1.fullname.split('.').length - 1];
                    info_1.name = info_1.fullname.split('.' + info_1.extension)[info_1.fullname.split('.' + info_1.extension).length - 2];
                    resolve(info_1);
                }).catch(function (err) {
                    reject(err);
                });
            }
            else {
                reject('file not exists');
            }
        });
    };
    FFVideoConvert.prototype.loadcmd = function (video, params) {
        var that = this;
        return new Promise(function (resolve, reject) {
            if (!params || !video) {
                reject('need more params');
            }
            else {
                that.info(video).then(function (info) {
                    if (!params.bin)
                        params.bin = that.bin;
                    if (!params.destinationPath)
                        params.destinationPath = that.destinationPath;
                    var cmd = ['-y', '-i', video,];
                    var name = info.name + '_conv';
                    if (params.name)
                        name = params.name;
                    var destfile = params.destinationPath + '/' + name;
                    var destfileconverted;
                    if (params.startTime) {
                        cmd.push('-ss');
                        cmd.push(params.startTime + '');
                        if (params.stopTime) {
                            cmd.push('-t');
                            cmd.push((params.stopTime - params.startTime) + '');
                        }
                        else if (params.duration) {
                            cmd.push('-t');
                            cmd.push(params.duration + '');
                        }
                    }
                    else if (params.duration) {
                        cmd.push('-t');
                        cmd.push(params.duration + '');
                    }
                    else if (params.stopTime) {
                        cmd.push('-t');
                        cmd.push((params.stopTime - params.startTime) + '');
                    }
                    if (!params.audioCodec && !params.videoCodec) {
                        cmd.push('-c:a');
                        cmd.push('copy');
                        cmd.push('-c:v');
                        cmd.push('copy');
                        if (info.extension && (info.extension === 'mp4' || info.extension === 'flv')) {
                            cmd.push('-f');
                            cmd.push('mp4');
                            destfileconverted = nodeFileGenSeries.genFileSequence(destfile + '.mp4');
                            cmd.push(destfileconverted);
                        }
                    }
                    else if (params.audioCodec === 'none' && !params.videoCodec) {
                        cmd.push('-c:v');
                        cmd.push('copy');
                    }
                    else if (params.videoCodec === 'none' && !params.audioCodec) {
                        cmd.push('-c:a');
                        cmd.push('copy');
                    }
                    else if (params.videoCodec === 'none' && params.audioCodec) {
                        cmd.push('-c:a');
                        cmd.push(params.audioCodec);
                    }
                    else if (params.audioCodec === 'none' && params.videoCodec) {
                        cmd.push('-c:v');
                        cmd.push(params.videoCodec);
                    }
                    else {
                        cmd.push('-c:a');
                        cmd.push(params.audioCodec);
                        cmd.push('-c:v');
                        cmd.push(params.videoCodec);
                    }
                    console.log(cmd);
                    resolve({ cmd: params.bin, options: cmd, destfile: destfileconverted });
                }).catch(function (err) {
                    reject(err);
                });
            }
        });
    };
    FFVideoConvert.prototype.convert = function (video, params) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var that = _this;
            that.loadcmd(video, params).then(function (p) {
                var conversion = spawn(p.cmd, p.options, { detached: true, stdio: 'ignore' });
                conversion.on("close", function (code, signal) {
                    console.log(code, signal);
                    if (code === 0) {
                        resolve(p.destfile);
                    }
                    else {
                        reject({ error: { code: code, signal: signal } });
                    }
                });
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    FFVideoConvert.prototype.extractAudio = function (video, destname) {
        var that = this;
        return new Promise(function (resolve, reject) {
            var newparams = { audioCodec: 'copy' };
            if (destname)
                newparams.name = destname;
            that.convert(video, newparams).then(function (a) {
                resolve(a);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    FFVideoConvert.prototype.extractVideo = function (video, destname) {
        var that = this;
        return new Promise(function (resolve, reject) {
            var newparams = { videoCodec: 'copy' };
            if (destname)
                newparams.name = destname;
            that.convert(video, newparams).then(function (a) {
                resolve(a);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    FFVideoConvert.prototype.cutVideo = function (video, params) {
        var that = this;
        return new Promise(function (resolve, reject) {
            var newparams = params;
            if (params.name)
                newparams.name = params.name;
            that.convert(video, newparams).then(function (a) {
                resolve(a);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    return FFVideoConvert;
}());
exports.FFVideoConvert = FFVideoConvert;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZGV4LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0EsNkNBQThDO0FBQzlDLHVCQUF3QjtBQUN4Qix3REFBeUQ7QUFFekQsa0NBQW1DO0FBRW5DLElBQU0sS0FBSyxHQUFHLGFBQWEsQ0FBQyxLQUFLLENBQUE7QUFFakMsSUFBTSxXQUFXLEdBQUcsT0FBTyxDQUFDLG9CQUFvQixDQUFDLENBQUM7QUFHbEQ7SUFJSSx3QkFBWSxJQUFtQjtRQUgvQixRQUFHLEdBQVcsUUFBUSxDQUFBO1FBQ3RCLG9CQUFlLEdBQVcsR0FBRyxDQUFBO1FBR3pCLElBQU0sSUFBSSxHQUFHLElBQUksQ0FBQTtRQUNqQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ1AsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztnQkFBQyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUE7WUFDakMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQztnQkFBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUE7UUFFekUsQ0FBQztJQUNMLENBQUM7SUFDRCxvQ0FBVyxHQUFYLFVBQVksS0FBYTtRQUNyQixNQUFNLENBQUMsSUFBSSxPQUFPLENBQVMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUN2QyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBQztnQkFDdEIsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFBO1lBQ2QsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsR0FBRztnQkFDVCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUE7WUFDZixDQUFDLENBQUMsQ0FBQTtRQUNOLENBQUMsQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUNELDZCQUFJLEdBQUosVUFBSyxLQUFhO1FBQ2QsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFBO1FBQ2pCLE1BQU0sQ0FBQyxJQUFJLE9BQU8sQ0FBYSxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQzNDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN2QixJQUFNLE1BQUksR0FBZSxFQUFFLENBQUE7Z0JBQzNCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsQ0FBQztvQkFDM0IsTUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUE7b0JBQ2pCLE1BQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQTtvQkFDN0QsTUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtvQkFDekMsTUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUE7b0JBQzlFLE1BQUksQ0FBQyxJQUFJLEdBQUcsTUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLE1BQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsTUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQTtvQkFDM0csT0FBTyxDQUFDLE1BQUksQ0FBQyxDQUFBO2dCQUNqQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxHQUFHO29CQUNULE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQTtnQkFDZixDQUFDLENBQUMsQ0FBQTtZQUNOLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixNQUFNLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtZQUM3QixDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBQ0QsZ0NBQU8sR0FBUCxVQUFRLEtBQWEsRUFBRSxNQUFvQjtRQUN2QyxJQUFNLElBQUksR0FBRyxJQUFJLENBQUE7UUFDakIsTUFBTSxDQUFDLElBQUksT0FBTyxDQUF1RCxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQ3JGLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDcEIsTUFBTSxDQUFDLGtCQUFrQixDQUFDLENBQUE7WUFDOUIsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUVKLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsSUFBSTtvQkFFdkIsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO3dCQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQTtvQkFDdEMsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDO3dCQUFDLE1BQU0sQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQTtvQkFDMUUsSUFBTSxHQUFHLEdBQUcsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxDQUFBO29CQUVoQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQTtvQkFHOUIsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQzt3QkFBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQTtvQkFFbkMsSUFBTSxRQUFRLEdBQUcsTUFBTSxDQUFDLGVBQWUsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFBO29CQUVwRCxJQUFJLGlCQUFpQixDQUFBO29CQUdyQixFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzt3QkFDbkIsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTt3QkFDZixHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLENBQUE7d0JBQy9CLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDOzRCQUNsQixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBOzRCQUNkLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQTt3QkFDdkQsQ0FBQzt3QkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7NEJBQ3pCLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7NEJBQ2QsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxDQUFBO3dCQUNsQyxDQUFDO29CQUVMLENBQUM7b0JBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO3dCQUN6QixHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO3dCQUNkLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUMsQ0FBQTtvQkFDbEMsQ0FBQztvQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7d0JBQ3pCLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7d0JBQ2QsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFBO29CQUN2RCxDQUFDO29CQUVELEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO3dCQUMzQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO3dCQUNoQixHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO3dCQUNoQixHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO3dCQUNoQixHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO3dCQUVoQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7NEJBQzNFLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7NEJBQ2QsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTs0QkFDZixpQkFBaUIsR0FBRyxpQkFBaUIsQ0FBQyxlQUFlLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxDQUFBOzRCQUN4RSxHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUE7d0JBQy9CLENBQUM7b0JBQ0wsQ0FBQztvQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsS0FBSyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzt3QkFDNUQsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTt3QkFDaEIsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtvQkFDcEIsQ0FBQztvQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsS0FBSyxNQUFNLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzt3QkFDNUQsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTt3QkFDaEIsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtvQkFDcEIsQ0FBQztvQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsS0FBSyxNQUFNLElBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7d0JBQzNELEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7d0JBQ2hCLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFBO29CQUMvQixDQUFDO29CQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBVSxLQUFLLE1BQU0sSUFBSSxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzt3QkFDM0QsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTt3QkFDaEIsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUE7b0JBQy9CLENBQUM7b0JBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ0osR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTt3QkFDaEIsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUE7d0JBQzNCLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7d0JBQ2hCLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFBO29CQUMvQixDQUFDO29CQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7b0JBQ2hCLE9BQU8sQ0FBQyxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsR0FBRyxFQUFFLE9BQU8sRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLGlCQUFpQixFQUFFLENBQUMsQ0FBQTtnQkFDM0UsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQUMsR0FBRztvQkFDVCxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUE7Z0JBQ2YsQ0FBQyxDQUFDLENBQUE7WUFJTixDQUFDO1FBR0wsQ0FBQyxDQUFDLENBQUE7SUFFTixDQUFDO0lBRUQsZ0NBQU8sR0FBUCxVQUFRLEtBQWEsRUFBRSxNQUFvQjtRQUEzQyxpQkFtQkM7UUFsQkcsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFTLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDdkMsSUFBTSxJQUFJLEdBQUcsS0FBSSxDQUFBO1lBQ2pCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFDLENBQUM7Z0JBQy9CLElBQU0sVUFBVSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFBO2dCQUMvRSxVQUFVLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFDLElBQUksRUFBRSxNQUFNO29CQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQTtvQkFDekIsRUFBRSxDQUFDLENBQUMsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2IsT0FBTyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQTtvQkFDdkIsQ0FBQztvQkFBQyxJQUFJLENBQUMsQ0FBQzt3QkFDSixNQUFNLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUE7b0JBQ3JELENBQUM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUE7WUFFTixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxHQUFHO2dCQUNULE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQTtZQUNmLENBQUMsQ0FBQyxDQUFBO1FBRU4sQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBQ0QscUNBQVksR0FBWixVQUFhLEtBQUssRUFBRSxRQUFpQjtRQUNqQyxJQUFNLElBQUksR0FBRyxJQUFJLENBQUE7UUFDakIsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDL0IsSUFBTSxTQUFTLEdBQWlCLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxDQUFBO1lBQ3RELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQztnQkFBQyxTQUFTLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQTtZQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFDO2dCQUNsQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDZCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxHQUFHO2dCQUNULE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQTtZQUNmLENBQUMsQ0FBQyxDQUFBO1FBQ04sQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBQ0QscUNBQVksR0FBWixVQUFhLEtBQUssRUFBRSxRQUFpQjtRQUNqQyxJQUFNLElBQUksR0FBRyxJQUFJLENBQUE7UUFDakIsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDL0IsSUFBTSxTQUFTLEdBQWlCLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxDQUFBO1lBQ3RELEVBQUUsQ0FBQyxDQUFDLFFBQVEsQ0FBQztnQkFBQyxTQUFTLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQTtZQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFDO2dCQUNsQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDZCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxHQUFHO2dCQUNULE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQTtZQUNmLENBQUMsQ0FBQyxDQUFBO1FBQ04sQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBQ0QsaUNBQVEsR0FBUixVQUFTLEtBQUssRUFBRSxNQUFrRjtRQUM5RixJQUFNLElBQUksR0FBRyxJQUFJLENBQUE7UUFDakIsTUFBTSxDQUFDLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDL0IsSUFBTSxTQUFTLEdBQWlCLE1BQU0sQ0FBQTtZQUN0QyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUFDLFNBQVMsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQTtZQUM3QyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxDQUFDO2dCQUNsQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUE7WUFDZCxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBQyxHQUFHO2dCQUNULE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQTtZQUNmLENBQUMsQ0FBQyxDQUFBO1FBQ04sQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBQ0wscUJBQUM7QUFBRCxDQXhMQSxBQXdMQyxJQUFBO0FBeExZLHdDQUFjIiwiZmlsZSI6ImluZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSWZmdmlkZW8sIElmZnZpZGVvQ29uZiwgSVZpZGVvUGFyYW1zLCBJdmlkZW9pbmZvIH0gZnJvbSAnLi9pbnRlcmZhY2VzL2ZmdmlkZW8nXG5pbXBvcnQgKiBhcyBjaGlsZF9wcm9jZXNzIGZyb20gJ2NoaWxkX3Byb2Nlc3MnXG5pbXBvcnQgKiBhcyBmcyBmcm9tICdmcydcbmltcG9ydCAqIGFzIG5vZGVGaWxlR2VuU2VyaWVzIGZyb20gJ25vZGUtZmlsZS1nZW4tc2VyaWVzJ1xuXG5pbXBvcnQgKiBhcyBQcm9taXNlIGZyb20gJ2JsdWViaXJkJ1xuXG5jb25zdCBzcGF3biA9IGNoaWxkX3Byb2Nlc3Muc3Bhd25cblxuY29uc3QgZ2V0RHVyYXRpb24gPSByZXF1aXJlKCdnZXQtdmlkZW8tZHVyYXRpb24nKTtcblxuXG5leHBvcnQgY2xhc3MgRkZWaWRlb0NvbnZlcnQgaW1wbGVtZW50cyBJZmZ2aWRlbyB7XG4gICAgYmluOiBzdHJpbmcgPSAnZmZtcGVnJ1xuICAgIGRlc3RpbmF0aW9uUGF0aDogc3RyaW5nID0gJy4nXG5cbiAgICBjb25zdHJ1Y3Rvcihjb25mPzogSWZmdmlkZW9Db25mKSB7XG4gICAgICAgIGNvbnN0IHRoYXQgPSB0aGlzXG4gICAgICAgIGlmIChjb25mKSB7XG4gICAgICAgICAgICBpZiAoY29uZi5iaW4pIHRoYXQuYmluID0gY29uZi5iaW5cbiAgICAgICAgICAgIGlmIChjb25mLmRlc3RpbmF0aW9uUGF0aCkgdGhhdC5kZXN0aW5hdGlvblBhdGggPSBjb25mLmRlc3RpbmF0aW9uUGF0aFxuXG4gICAgICAgIH1cbiAgICB9XG4gICAgZ2V0RHVyYXRpb24odmlkZW86IHN0cmluZykge1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2U8bnVtYmVyPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBnZXREdXJhdGlvbih2aWRlbykudGhlbigoYSkgPT4ge1xuICAgICAgICAgICAgICAgIHJlc29sdmUoYSlcbiAgICAgICAgICAgIH0pLmNhdGNoKChlcnIpID0+IHtcbiAgICAgICAgICAgICAgICByZWplY3QoZXJyKVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSlcbiAgICB9XG4gICAgaW5mbyh2aWRlbzogc3RyaW5nKSB7XG4gICAgICAgIGNvbnN0IHRoYXQgPSB0aGlzXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxJdmlkZW9pbmZvPigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBpZiAoZnMuZXhpc3RzU3luYyh2aWRlbykpIHtcbiAgICAgICAgICAgICAgICBjb25zdCBpbmZvID0gPEl2aWRlb2luZm8+e31cbiAgICAgICAgICAgICAgICB0aGF0LmdldER1cmF0aW9uKHZpZGVvKS50aGVuKChhKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGluZm8uZHVyYXRpb24gPSBhXG4gICAgICAgICAgICAgICAgICAgIGluZm8uZnVsbG5hbWUgPSB2aWRlby5zcGxpdCgnLycpW3ZpZGVvLnNwbGl0KCcvJykubGVuZ3RoIC0gMV1cbiAgICAgICAgICAgICAgICAgICAgaW5mby5wYXRoID0gdmlkZW8uc3BsaXQoaW5mby5mdWxsbmFtZSlbMF1cbiAgICAgICAgICAgICAgICAgICAgaW5mby5leHRlbnNpb24gPSBpbmZvLmZ1bGxuYW1lLnNwbGl0KCcuJylbaW5mby5mdWxsbmFtZS5zcGxpdCgnLicpLmxlbmd0aCAtIDFdXG4gICAgICAgICAgICAgICAgICAgIGluZm8ubmFtZSA9IGluZm8uZnVsbG5hbWUuc3BsaXQoJy4nICsgaW5mby5leHRlbnNpb24pW2luZm8uZnVsbG5hbWUuc3BsaXQoJy4nICsgaW5mby5leHRlbnNpb24pLmxlbmd0aCAtIDJdXG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoaW5mbylcbiAgICAgICAgICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpXG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmVqZWN0KCdmaWxlIG5vdCBleGlzdHMnKVxuICAgICAgICAgICAgfVxuICAgICAgICB9KVxuICAgIH1cbiAgICBsb2FkY21kKHZpZGVvOiBzdHJpbmcsIHBhcmFtczogSVZpZGVvUGFyYW1zKSB7XG4gICAgICAgIGNvbnN0IHRoYXQgPSB0aGlzXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTx7IGNtZDogc3RyaW5nLCBvcHRpb25zOiBzdHJpbmdbXSwgZGVzdGZpbGU6IHN0cmluZyB9PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBpZiAoIXBhcmFtcyB8fCAhdmlkZW8pIHtcbiAgICAgICAgICAgICAgICByZWplY3QoJ25lZWQgbW9yZSBwYXJhbXMnKVxuICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgIHRoYXQuaW5mbyh2aWRlbykudGhlbigoaW5mbykgPT4ge1xuXG4gICAgICAgICAgICAgICAgICAgIGlmICghcGFyYW1zLmJpbikgcGFyYW1zLmJpbiA9IHRoYXQuYmluXG4gICAgICAgICAgICAgICAgICAgIGlmICghcGFyYW1zLmRlc3RpbmF0aW9uUGF0aCkgcGFyYW1zLmRlc3RpbmF0aW9uUGF0aCA9IHRoYXQuZGVzdGluYXRpb25QYXRoXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNtZCA9IFsnLXknLCAnLWknLCB2aWRlbyxdXG5cbiAgICAgICAgICAgICAgICAgICAgbGV0IG5hbWUgPSBpbmZvLm5hbWUgKyAnX2NvbnYnXG5cblxuICAgICAgICAgICAgICAgICAgICBpZiAocGFyYW1zLm5hbWUpIG5hbWUgPSBwYXJhbXMubmFtZVxuXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGRlc3RmaWxlID0gcGFyYW1zLmRlc3RpbmF0aW9uUGF0aCArICcvJyArIG5hbWVcblxuICAgICAgICAgICAgICAgICAgICBsZXQgZGVzdGZpbGVjb252ZXJ0ZWRcblxuXG4gICAgICAgICAgICAgICAgICAgIGlmIChwYXJhbXMuc3RhcnRUaW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjbWQucHVzaCgnLXNzJylcbiAgICAgICAgICAgICAgICAgICAgICAgIGNtZC5wdXNoKHBhcmFtcy5zdGFydFRpbWUgKyAnJylcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChwYXJhbXMuc3RvcFRpbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbWQucHVzaCgnLXQnKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNtZC5wdXNoKChwYXJhbXMuc3RvcFRpbWUgLSBwYXJhbXMuc3RhcnRUaW1lKSArICcnKVxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChwYXJhbXMuZHVyYXRpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbWQucHVzaCgnLXQnKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNtZC5wdXNoKHBhcmFtcy5kdXJhdGlvbiArICcnKVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocGFyYW1zLmR1cmF0aW9uKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjbWQucHVzaCgnLXQnKVxuICAgICAgICAgICAgICAgICAgICAgICAgY21kLnB1c2gocGFyYW1zLmR1cmF0aW9uICsgJycpXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocGFyYW1zLnN0b3BUaW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjbWQucHVzaCgnLXQnKVxuICAgICAgICAgICAgICAgICAgICAgICAgY21kLnB1c2goKHBhcmFtcy5zdG9wVGltZSAtIHBhcmFtcy5zdGFydFRpbWUpICsgJycpXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICBpZiAoIXBhcmFtcy5hdWRpb0NvZGVjICYmICFwYXJhbXMudmlkZW9Db2RlYykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY21kLnB1c2goJy1jOmEnKVxuICAgICAgICAgICAgICAgICAgICAgICAgY21kLnB1c2goJ2NvcHknKVxuICAgICAgICAgICAgICAgICAgICAgICAgY21kLnB1c2goJy1jOnYnKVxuICAgICAgICAgICAgICAgICAgICAgICAgY21kLnB1c2goJ2NvcHknKVxuXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaW5mby5leHRlbnNpb24gJiYgKGluZm8uZXh0ZW5zaW9uID09PSAnbXA0JyB8fCBpbmZvLmV4dGVuc2lvbiA9PT0gJ2ZsdicpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY21kLnB1c2goJy1mJylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbWQucHVzaCgnbXA0JylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkZXN0ZmlsZWNvbnZlcnRlZCA9IG5vZGVGaWxlR2VuU2VyaWVzLmdlbkZpbGVTZXF1ZW5jZShkZXN0ZmlsZSArICcubXA0JylcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbWQucHVzaChkZXN0ZmlsZWNvbnZlcnRlZClcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChwYXJhbXMuYXVkaW9Db2RlYyA9PT0gJ25vbmUnICYmICFwYXJhbXMudmlkZW9Db2RlYykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY21kLnB1c2goJy1jOnYnKVxuICAgICAgICAgICAgICAgICAgICAgICAgY21kLnB1c2goJ2NvcHknKVxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHBhcmFtcy52aWRlb0NvZGVjID09PSAnbm9uZScgJiYgIXBhcmFtcy5hdWRpb0NvZGVjKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjbWQucHVzaCgnLWM6YScpXG4gICAgICAgICAgICAgICAgICAgICAgICBjbWQucHVzaCgnY29weScpXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocGFyYW1zLnZpZGVvQ29kZWMgPT09ICdub25lJyAmJiBwYXJhbXMuYXVkaW9Db2RlYykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY21kLnB1c2goJy1jOmEnKVxuICAgICAgICAgICAgICAgICAgICAgICAgY21kLnB1c2gocGFyYW1zLmF1ZGlvQ29kZWMpXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocGFyYW1zLmF1ZGlvQ29kZWMgPT09ICdub25lJyAmJiBwYXJhbXMudmlkZW9Db2RlYykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY21kLnB1c2goJy1jOnYnKVxuICAgICAgICAgICAgICAgICAgICAgICAgY21kLnB1c2gocGFyYW1zLnZpZGVvQ29kZWMpXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjbWQucHVzaCgnLWM6YScpXG4gICAgICAgICAgICAgICAgICAgICAgICBjbWQucHVzaChwYXJhbXMuYXVkaW9Db2RlYylcbiAgICAgICAgICAgICAgICAgICAgICAgIGNtZC5wdXNoKCctYzp2JylcbiAgICAgICAgICAgICAgICAgICAgICAgIGNtZC5wdXNoKHBhcmFtcy52aWRlb0NvZGVjKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGNtZClcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh7IGNtZDogcGFyYW1zLmJpbiwgb3B0aW9uczogY21kLCBkZXN0ZmlsZTogZGVzdGZpbGVjb252ZXJ0ZWQgfSlcbiAgICAgICAgICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpXG4gICAgICAgICAgICAgICAgfSlcblxuXG5cbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgIH0pXG5cbiAgICB9XG5cbiAgICBjb252ZXJ0KHZpZGVvOiBzdHJpbmcsIHBhcmFtczogSVZpZGVvUGFyYW1zKSB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxzdHJpbmc+KChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHRoYXQgPSB0aGlzXG4gICAgICAgICAgICB0aGF0LmxvYWRjbWQodmlkZW8sIHBhcmFtcykudGhlbigocCkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IGNvbnZlcnNpb24gPSBzcGF3bihwLmNtZCwgcC5vcHRpb25zLCB7IGRldGFjaGVkOiB0cnVlLCBzdGRpbzogJ2lnbm9yZScgfSlcbiAgICAgICAgICAgICAgICBjb252ZXJzaW9uLm9uKFwiY2xvc2VcIiwgKGNvZGUsIHNpZ25hbCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhjb2RlLCBzaWduYWwpXG4gICAgICAgICAgICAgICAgICAgIGlmIChjb2RlID09PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHAuZGVzdGZpbGUpXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWplY3QoeyBlcnJvcjogeyBjb2RlOiBjb2RlLCBzaWduYWw6IHNpZ25hbCB9IH0pXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9KVxuXG4gICAgICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XG4gICAgICAgICAgICAgICAgcmVqZWN0KGVycilcbiAgICAgICAgICAgIH0pXG5cbiAgICAgICAgfSlcbiAgICB9XG4gICAgZXh0cmFjdEF1ZGlvKHZpZGVvLCBkZXN0bmFtZT86IHN0cmluZykge1xuICAgICAgICBjb25zdCB0aGF0ID0gdGhpc1xuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgbmV3cGFyYW1zOiBJVmlkZW9QYXJhbXMgPSB7IGF1ZGlvQ29kZWM6ICdjb3B5JyB9XG4gICAgICAgICAgICBpZiAoZGVzdG5hbWUpIG5ld3BhcmFtcy5uYW1lID0gZGVzdG5hbWVcbiAgICAgICAgICAgIHRoYXQuY29udmVydCh2aWRlbywgbmV3cGFyYW1zKS50aGVuKChhKSA9PiB7XG4gICAgICAgICAgICAgICAgcmVzb2x2ZShhKVxuICAgICAgICAgICAgfSkuY2F0Y2goKGVycikgPT4ge1xuICAgICAgICAgICAgICAgIHJlamVjdChlcnIpXG4gICAgICAgICAgICB9KVxuICAgICAgICB9KVxuICAgIH1cbiAgICBleHRyYWN0VmlkZW8odmlkZW8sIGRlc3RuYW1lPzogc3RyaW5nKSB7XG4gICAgICAgIGNvbnN0IHRoYXQgPSB0aGlzXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBuZXdwYXJhbXM6IElWaWRlb1BhcmFtcyA9IHsgdmlkZW9Db2RlYzogJ2NvcHknIH1cbiAgICAgICAgICAgIGlmIChkZXN0bmFtZSkgbmV3cGFyYW1zLm5hbWUgPSBkZXN0bmFtZVxuICAgICAgICAgICAgdGhhdC5jb252ZXJ0KHZpZGVvLCBuZXdwYXJhbXMpLnRoZW4oKGEpID0+IHtcbiAgICAgICAgICAgICAgICByZXNvbHZlKGEpXG4gICAgICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB7XG4gICAgICAgICAgICAgICAgcmVqZWN0KGVycilcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0pXG4gICAgfVxuICAgIGN1dFZpZGVvKHZpZGVvLCBwYXJhbXM6IHsgc3RhcnRUaW1lOiBudW1iZXIsIGR1cmF0aW9uPzogbnVtYmVyLCBzdG9wVGltZT86IG51bWJlciwgbmFtZT86IHN0cmluZyB9KSB7XG4gICAgICAgIGNvbnN0IHRoYXQgPSB0aGlzXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBuZXdwYXJhbXM6IElWaWRlb1BhcmFtcyA9IHBhcmFtc1xuICAgICAgICAgICAgaWYgKHBhcmFtcy5uYW1lKSBuZXdwYXJhbXMubmFtZSA9IHBhcmFtcy5uYW1lXG4gICAgICAgICAgICB0aGF0LmNvbnZlcnQodmlkZW8sIG5ld3BhcmFtcykudGhlbigoYSkgPT4ge1xuICAgICAgICAgICAgICAgIHJlc29sdmUoYSlcbiAgICAgICAgICAgIH0pLmNhdGNoKChlcnIpID0+IHtcbiAgICAgICAgICAgICAgICByZWplY3QoZXJyKVxuICAgICAgICAgICAgfSlcbiAgICAgICAgfSlcbiAgICB9XG59Il19
